import fetch from 'node-fetch'
import merge from 'merge'
import readPackageJson from 'read-pkg-up'

import Debug from './debug'


const debug = Debug('framedl:fetch')

const DEFAULT_OPTIONS = {
  headers: {
    'User-Agent': (() => {
      const pkg = readPackageJson.sync(__dirname)
      return `${pkg.name}/${pkg.version} (+${pkg.homepage}) node/${process.version}`
    })()
  }
}


export default function notFetch(url, options) {
  debug('Fetching "%s"', url)
  return fetch(url, merge(DEFAULT_OPTIONS, options))
}
