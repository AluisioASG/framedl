import querystring from 'querystring'
import url from 'url'

import cheerio from 'cheerio'

import fetch from './fetch'


const MOONBASE_VIEWER_URL = 'http://moonbase.chirpingmustard.com/aftertime/viewer?'


export default async function(story, enhanceMethod, frameNumber) {
  const qs = querystring.stringify({story, e: enhanceMethod, f: frameNumber})
  const response = await fetch(MOONBASE_VIEWER_URL + qs)

  const $ = cheerio.load(await response.text())
  const $imageRow = $('table tr:nth-child(1)'),
        $enhanceRow = $('table tr:nth-child(2)'),
        $framesRow = $('table tr:nth-child(3)'),
        $bbcodeLink = $('table td:only-of-type a:only-of-type')
                      .filter((i, e) => $(e).text() === 'bbcode')

  return {
    firstFrame: Number($framesRow.find('td:first-of-type').text().replace(/[^0-9]/g, '')),
    lastFrame: Number($framesRow.find('td:last-of-type').text().replace(/[^0-9]/g, '')),
    enhanceMethods: $enhanceRow.find('a, b').map((i, e) => $(e).text()).get(),
    current: {
      frameNumber: Number(url.parse($bbcodeLink.attr('href'), true).query.f),
      enhanceMethod: $enhanceRow.find('b').text(),
      imageUrl: url.resolve(response.url, $imageRow.find('img').attr('src')),
    }
  }
}
