import debug from 'debug'
import printf from 'printf'


export default function DebugFormatted(namespace) {
  const realDebug = debug(namespace)
  return (...args) => realDebug(printf(...args))
}
