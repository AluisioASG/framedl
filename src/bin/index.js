import fs from 'fs'

import findUp from 'find-up'
import gm from 'gm'
import printf from 'printf'
import toml from 'toml'
import yargs from 'yargs'

import Debug from '../lib/debug'
import fetch from '../lib/fetch'
import fetchStoryData from '../lib/storypage'


const debug = Debug('framedl')


function statPromise(filepath) {
  return new Promise((resolve, reject) => {
    fs.stat(filepath, (err, stat) => err ? reject(err) : resolve(stat))
  })
}

async function checkAndFetchLatestFrame(config) {
  const story = await fetchStoryData(config.story, config.enhance)
  debug('Story information: %O', story)
  let last
  if (story.lastFrame === story.current.frameNumber) {
    debug('Index frame is last frame')
    last = story.current
  } else {
    last = (await fetchStoryData(config.story, config.enhance, story.lastFrame)).current
  }
  debug('Information for last frame: %O', last)

  const destination = printf(config.destination, last.frameNumber)
  const fileTime = await statPromise(destination).then(stat => stat.mtime, e => {
    if (e.code !== 'ENOENT') throw e
    return null
  })

  const frameResponse = await fetch(last.imageUrl)
  const frameLastModifiedIn = frameResponse.headers.get('Last-Modified')
  const frameTime = frameLastModifiedIn ? new Date(frameLastModifiedIn) : null
  debug('Frame %04d: file time "%s", frame time "%s"', last.frameNumber, fileTime, frameTime)
  if (!fileTime || !frameTime || fileTime < frameTime) {
    debug('Writing frame %04d to "%s"', last.frameNumber, destination)
    gm(frameResponse.body)
      .font(findUp.sync('xkcd_beanish-Regular.otf', {cwd: __dirname}), 12)
      .fill('#ffffff')
      .drawText(2, 2, printf('%s_%04d', config.story, last.frameNumber), 'southwest')
      .stream('png').pipe(fs.createWriteStream(destination, 'binary'))
  }
}


const [configFile, configSection] = yargs
                                    .usage('Usage: $0 CONFIG_FILE SELECTOR')
                                    .demand(2)
                                    .argv._
debug('Using config "%s":"%s"', configFile, configSection)

const config = toml.parse(fs.readFileSync(configFile))[configSection]
checkAndFetchLatestFrame(config).catch(err => process.nextTick(() => { throw err }))
